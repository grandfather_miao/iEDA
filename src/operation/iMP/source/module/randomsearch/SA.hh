/**
 * @file SA.hh
 * @author Fuxing Huang (fxxhuang@gmail.com)
 * @brief
 * @version 0.1
 * @date 2023-07-12
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef IMP_SA_H
#define IMP_SA_H
#include "Annealer.hh"
#include "NetList.hh"
namespace imp {
void SeqPairbaseSA();

}  // namespace imp

#endif